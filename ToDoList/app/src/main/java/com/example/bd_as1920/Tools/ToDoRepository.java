package com.example.bd_as1920.Tools;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.bd_as1920.BD.ToDoDAO;
import com.example.bd_as1920.BD.ToDoListDataBase;
import com.example.bd_as1920.Models.ToDo;

import java.util.List;

//Repository de l'entité ToDo


public class ToDoRepository {

    private ToDoDAO toDoDao;
    private LiveData<List<ToDo>> toDoListLD;
    private LiveData<Integer> nbToDoListLD;

    public ToDoRepository(Application application){
        ToDoListDataBase toDoListDataBase = ToDoListDataBase.getDataBase(application);
        toDoDao = toDoListDataBase.getToDoDao();
        toDoListLD = toDoDao.getToDoListLD();
        nbToDoListLD = toDoDao.countLD();

    }


    public Long insert (ToDo toDo){
        try {
            return new InsertAsync(toDoDao).execute(toDo).get();
        }catch (Exception e){
            Log.d("MesLogs", "pb Insertion ToDo");
            return null;
        }
    }

    private static class InsertAsync extends AsyncTask<ToDo, Void, Long> {
        private ToDoDAO toDoDAO;

        public InsertAsync (ToDoDAO t){
            toDoDAO = t;
        }

        @Override
        protected Long doInBackground(ToDo... toDos){
            return toDoDAO.insert(toDos[0]);
        }
    }

    public void update (final ToDo toDo){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                toDoDao.update(toDo);
            }
        });
    }

    public void delete (final ToDo toDo){
        new DeleteAsync(toDoDao).execute(toDo);
    }

    private static class DeleteAsync extends AsyncTask<ToDo, Void, Void>{
        private ToDoDAO toDoDAO;

        public DeleteAsync (ToDoDAO t){
            toDoDAO = t;
        }

        @Override
        protected Void doInBackground(ToDo... toDos){
            toDoDAO.delete(toDos[0]);
            return null;
        }
    }

    public void deleteAll (){
        new DeleteAllAsync(toDoDao).execute();
        Log.i("ToDoRepository", "Invoked deleteAll");
    }

    private static class DeleteAllAsync extends AsyncTask<Void, Void, Void>{
        private ToDoDAO toDoDAO;

        public DeleteAllAsync (ToDoDAO t){
            toDoDAO = t;
            Log.i("ToDoRepository", "Invoked DeleteAllAsync");
        }

        @Override
        protected Void doInBackground(Void ... args){
            toDoDAO.deleteAll();
            Log.i("ToDoRepository", "Invoked doInBackground");
            return null;
        }

    }

    public Integer count (){
        try {
            return new CountAsync(toDoDao).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb count");
        }
        return null;
    }

    private static class CountAsync extends AsyncTask<Void, Void, Integer>{
        private ToDoDAO toDoDAO;

        public CountAsync (ToDoDAO t){
            toDoDAO = t;
        }

        @Override
        protected Integer doInBackground(Void ... args){
            return toDoDAO.count();
        }
    }

    public LiveData<List<ToDo>> getToDoListLD() {
        return toDoListLD;
    }

    public LiveData<Integer> getNbToDoListLD() {
        return nbToDoListLD;
    }

    public List<ToDo> getToDoList(){
        try{
            return new GetTasksAsync(toDoDao).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb liste de tâches");
        }
        return null;
    }

    private static class GetTasksAsync extends AsyncTask<Void, Void, List<ToDo>>{
        private ToDoDAO toDoDAO;

        public GetTasksAsync (ToDoDAO t){
            toDoDAO = t;
        }

        @Override
        protected List<ToDo> doInBackground(Void ... args){
            return toDoDAO.getToDoLists();
        }
    }

    public List<ToDo> getListToDo(int groupId){
        try {
            return  new GetTaskAsyncGroup(toDoDao,groupId).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb liste de tâches en fonction du groupe");
        }
        return null;
    }

    private static class GetTaskAsyncGroup extends AsyncTask<Void, Void, List<ToDo>>{
        private ToDoDAO toDoDAO;
        private int groupId;

        public GetTaskAsyncGroup(ToDoDAO t, int groupeId) {
            toDoDAO = t;
            this.groupId = groupeId;
        }

        @Override
        protected  List<ToDo> doInBackground(Void ... arg){
            return toDoDAO.findToDoForGroup(groupId);
        }
    }
}