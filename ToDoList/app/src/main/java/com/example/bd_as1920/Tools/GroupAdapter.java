package com.example.bd_as1920.Tools;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bd_as1920.Models.Group;
import com.example.bd_as1920.R;

import java.util.List;

//Adapteur utiliser dans la listView de la popUp d'ajout d'une nouvelle tâche

public class GroupAdapter extends ArrayAdapter<Group> {
    //groups est la list à afficher
    public GroupAdapter(Context context, List<Group> groups){
        super(context, 0, groups);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.button, parent, false);
        }
        GroupViewHolder groupViewHolder = (GroupViewHolder) convertView.getTag();
        if(groupViewHolder == null){
            //recuperation et stockage des TextView
            groupViewHolder = new GroupViewHolder();
            groupViewHolder.button = (TextView) convertView.findViewById(R.id.buttonTextView);

            convertView.setTag(groupViewHolder);
        }
        //getItem(position) va récupérer l'item [position] de la List<Group> groups
        Group group = getItem(position);
        //Remplissage des vue avec les information du group
        groupViewHolder.button.setText("<-- " + group.getColor() +" --> ");
        groupViewHolder.button.setTextColor(Color.parseColor(group.getColor()));


        return convertView;
    }

    private class GroupViewHolder{
        public TextView button;
    }
}
