package com.example.bd_as1920.Models;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.Comparator;

import static androidx.room.ForeignKey.CASCADE;

//Entité Todo Composer de d'un id d'un titre d'une description, d'une priorité et d'une id qui fait reference a l'id d'un groupe
@Entity(tableName = "ToDo_table",
        foreignKeys = @ForeignKey(
                entity = Group.class,
                parentColumns = "groupId",
                childColumns = "toDoIdGroup",
                onDelete = CASCADE
        ))
public class ToDo {
    @PrimaryKey(autoGenerate = true)
    public int toDoId;
    private String title;
    private String description;
    private int priority;
    private int toDoIdGroup;

    public ToDo(String title, String description, int priority,int toDoIdGroup) {
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.toDoIdGroup =toDoIdGroup;
    }

    public int getToDoIdGroup() {
        return toDoIdGroup;
    }

    public void setToDoIdGroup(int toDoIdGroup) {
        this.toDoIdGroup = toDoIdGroup;
    }

    public int getId() {
        return toDoId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public void setId(int id) {
        this.toDoId = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Tâche{" +
                "id=" + toDoId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                '}';
    }

    //Comparateur utiliser pour le trie de la list

    public static Comparator<ToDo> ComparatorTitle = new Comparator<ToDo>() {
        @Override
        public int compare(ToDo o1, ToDo o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }
    };

    public static Comparator<ToDo> ComparatorPriority = new Comparator<ToDo>() {
        @Override
        public int compare(ToDo o1, ToDo o2) {
            return o2.getPriority() - o1.getPriority();
        }
    };
}
