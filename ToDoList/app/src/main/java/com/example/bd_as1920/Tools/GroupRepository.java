package com.example.bd_as1920.Tools;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.example.bd_as1920.BD.GroupDAO;
import com.example.bd_as1920.BD.ToDoListDataBase;
import com.example.bd_as1920.Models.Group;

import java.util.List;

//Repository de l'entité Group

public class GroupRepository {
    private GroupDAO groupDao;

    public GroupRepository(Application application){
        ToDoListDataBase toDoListDataBase = ToDoListDataBase.getDataBase(application);
        groupDao = toDoListDataBase.getGroupDao();
    }


    public Long insert (Group group){
        try {
            return new GroupRepository.InsertAsync(groupDao).execute(group).get();
        }catch (Exception e){
            Log.d("MesLogs", "pb Insertion Group");
            return null;
        }
    }

    private static class InsertAsync extends AsyncTask<Group, Void, Long> {
        private GroupDAO groupDAO;

        public InsertAsync (GroupDAO t){
            groupDAO = t;
        }

        @Override
        protected Long doInBackground(Group... groups){
            return groupDAO.insert(groups[0]);
        }
    }

    public void update (final Group group){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                groupDao.update(group);
            }
        });
    }

    public void delete (final Group group){
        new GroupRepository.DeleteAsync(groupDao).execute(group);
    }

    private static class DeleteAsync extends AsyncTask<Group, Void, Void>{
        private GroupDAO groupDAO;

        public DeleteAsync (GroupDAO t){
            groupDAO = t;
        }

        @Override
        protected Void doInBackground(Group... groups){
            groupDAO.delete(groups[0]);
            return null;
        }
    }

    public void deleteAll (){
        new GroupRepository.DeleteAllAsync(groupDao).execute();
    }

    private static class DeleteAllAsync extends AsyncTask<Void, Void, Void>{
        private GroupDAO groupDAO;

        public DeleteAllAsync (GroupDAO t){
            groupDAO = t;
        }

        @Override
        protected Void doInBackground(Void ... args){
            groupDAO.deleteAll();
            return null;
        }

    }

    public List<Group> getGroups(){
        try{
            return new GroupRepository.GetTasksAsync(groupDao).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb liste de tâches");
        }
        return null;
    }

    private static class GetTasksAsync extends AsyncTask<Void, Void, List<Group>>{
        private GroupDAO groupDAO;

        public GetTasksAsync (GroupDAO t){
            groupDAO = t;
        }

        @Override
        protected List<Group> doInBackground(Void ... args){
            return groupDAO.getGroups();
        }
    }

}
