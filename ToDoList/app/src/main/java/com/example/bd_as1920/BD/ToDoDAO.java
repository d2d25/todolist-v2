package com.example.bd_as1920.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.bd_as1920.Models.ToDo;

import java.util.List;

//Requète sur la table Todo_table

@Dao
public interface ToDoDAO {
    @Insert
    Long insert(ToDo toDo); //

    @Update
    void update(ToDo toDo);

    @Delete
    void delete(ToDo toDo);

    @Query("Delete FROM Todo_table")
    void deleteAll();

    @Query("SELECT count(*) FROM Todo_table")
    int count();

    @Query("SELECT count(*) from Todo_table")
    LiveData<Integer> countLD();

    @Query("Select * from Todo_table order by priority DESC")
    List<ToDo> getToDoLists();

    @Query("Select * from Todo_table order by priority DESC")
    LiveData<List<ToDo>> getToDoListLD();

    @Query("select * FROM ToDo_table where toDoIdGroup =:toDoIdGroup")
    List<ToDo> findToDoForGroup(int toDoIdGroup);

}
