package com.example.bd_as1920.Tools;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bd_as1920.Models.Group;
import com.example.bd_as1920.Models.ToDo;
import com.example.bd_as1920.R;

import java.util.List;

//Adapter de la ListView du main

public class ToDoAdapter extends ArrayAdapter<ToDo> {
    //La list des groupes pour avoir la couleur du group corespondant au todo
    List<Group> groups;
    //toDoList est la list à afficher
    public  ToDoAdapter(Context context, List<ToDo> toDoList, List<Group> groups){
        super(context, 0 , toDoList);
        this.groups = groups;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.todolist_view, parent, false);
        }
        ToDoViewHolder toDoViewHolder = (ToDoViewHolder) convertView.getTag();
        if (toDoViewHolder == null) {
            //recuperation et stockage des TextView
            toDoViewHolder = new ToDoViewHolder();
            toDoViewHolder.text = (TextView) convertView.findViewById(R.id.text);
            toDoViewHolder.priority = (TextView) convertView.findViewById(R.id.priority);
            toDoViewHolder.description = (TextView) convertView.findViewById(R.id.description);

            convertView.setTag(toDoViewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Todo> toDoList
        ToDo toDo = getItem(position);
        //Initialisation sur blanc le temp de trouver le group corespondant au todo
        Group group = new Group("white");
        //recherche du group corespondant au todo
        for(Group group1 : groups){
            if(group1.getId() == toDo.getToDoIdGroup()){
                group = group1;
                break;
            }
        }
        //Remplissage des vue avec les information du todo
        toDoViewHolder.text.setText(toDo.getTitle());
        toDoViewHolder.priority.setText(String.valueOf(toDo.getPriority()));
        toDoViewHolder.description.setText(toDo.getDescription());
        //coloration de la ligne a la couleur de group corespondant
        convertView.setBackgroundColor(Color.parseColor(group.getColor()));

        return  convertView;
    }

    private class ToDoViewHolder {
        public TextView text;
        public TextView priority;
        public TextView description;

    }
}
