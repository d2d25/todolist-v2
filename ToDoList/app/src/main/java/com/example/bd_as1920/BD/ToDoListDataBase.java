package com.example.bd_as1920.BD;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.bd_as1920.Models.Group;
import com.example.bd_as1920.Models.ToDo;

//Création de base de données

@Database(entities = {Group.class, ToDo.class}, version = 1)
public abstract class ToDoListDataBase extends RoomDatabase {
    public abstract ToDoDAO getToDoDao();
    public abstract GroupDAO getGroupDao();

    private static ToDoListDataBase INSTANCE;

    public static ToDoListDataBase getDataBase(final Context context){

        if (INSTANCE == null){
            synchronized (ToDoListDataBase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ToDoListDataBase.class,"ToDoList_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}

