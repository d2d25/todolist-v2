package com.example.bd_as1920.Tools;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.bd_as1920.R;

//Personalisation d'une PopUp pour ajouter de nouveau ToDo

public class PopUpAjouter extends Dialog {
    private String title;
    private Button ajouterButton;
    private Button annulerButton;
    private TextView titleView;
    private EditText titreToDoEdit;
    private EditText decriptionToDoEdit;
    private EditText priorityToDoEdit;
    private ListView listViewGroup;

    public PopUpAjouter(@NonNull Context context) {
        super(context, R.style.Theme_AppCompat_Light_Dialog_Alert);
        setContentView(R.layout.popup_ajouter);
        //initialisation du Titre
        this.title = "Ajouter une Tâche";
        //recuperation et stockage des View
        this.ajouterButton = findViewById(R.id.ajouter_ToDo_PopUpAjouter);
        this.annulerButton = findViewById(R.id.annuler_ToDo_PopUpAjouter);
        this.titleView = findViewById(R.id.title_popup_ajouter);
        this.titreToDoEdit = findViewById(R.id.title_ToDo_PopUpAjouter);
        this.decriptionToDoEdit = findViewById(R.id.description_ToDo_PopUpAjouter);
        this.priorityToDoEdit = findViewById(R.id.priority_ToDo_PopUpAjouter);
        this.listViewGroup = findViewById(R.id.listViewGroup);

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Button getAjouterButton() {
        return ajouterButton;
    }

    public Button getAnnulerButton() {
        return annulerButton;
    }

    public EditText getTitreToDoEdit() {
        return titreToDoEdit;
    }

    public EditText getDecriptionToDoEdit() {
        return decriptionToDoEdit;
    }

    public EditText getPriorityToDoEdit() {
        return priorityToDoEdit;
    }

    public ListView getListViewGroup(){return  listViewGroup;}

    //Construction de la popUp
    public void build(){
        show();
        titleView.setText(title);

    }
}
