package com.example.bd_as1920.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.bd_as1920.Models.Group;

import java.util.List;

//Requète sur la table Group_table

@Dao
public interface GroupDAO {
    @Insert
    Long insert(Group group);

    @Update
    void update(Group group);

    @Delete
    void delete(Group group);

    @Query("Delete FROM Group_table")
    void deleteAll();

    @Query("Select * from Group_table")
    List<Group> getGroups();

}
