package com.example.bd_as1920;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.bd_as1920.Models.Group;
import com.example.bd_as1920.Models.ToDo;
import com.example.bd_as1920.Tools.GroupAdapter;
import com.example.bd_as1920.Tools.GroupViewModel;
import com.example.bd_as1920.Tools.PopUpAjouter;
import com.example.bd_as1920.Tools.ToDoAdapter;
import com.example.bd_as1920.Tools.ToDoViewModel;
import com.example.bd_as1920.Tools.TrieTodo;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    //param

    //ViewModel
    ToDoViewModel toDoViewModel;
    GroupViewModel groupViewModel;

    //List objects
    List<ToDo> toDoList;
    List<Group> groups;

    //listView
    ListView toDoListView;
    ListView groupListView;

    //Adapter
    ToDoAdapter toDoAdapter;
    GroupAdapter groupAdapter;


    //other
    PopUpAjouter popUpAjouter;
    TrieTodo trieTodo;
    Group groupDefault;
    Group groupChoisie;
    String groupColorDefault;
    boolean filtre;
    int indiceFiltre;
    Button buttonFiltre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialisation

        //ViewModel
        toDoViewModel = new ViewModelProvider(this).get(ToDoViewModel.class);
        groupViewModel = new ViewModelProvider(this).get(GroupViewModel.class);

        //List objects

        groups = groupViewModel.getGroups();

        //initialisation de la couleur par default si non presente dans la bdd création
        groupColorDefault = "white";
        groupDefault = new Group(groupColorDefault);
        if(groups.contains(groupDefault)){
            groupDefault = groups.get(groups.indexOf(groupDefault));
        }else{
            this.ajouter(new Group(groupColorDefault));
            groupDefault = groups.get(groups.indexOf(groupDefault));
        }
        groupChoisie = groupDefault;

        //initialisation des Groupes si bdd n'a que la couleur par default
        if(groups.size() < 2){
            List<Group> tempGroups = this.genererGroups();
            for (Group group : tempGroups){
                groupViewModel.insert(group);
            }
            groups =groupViewModel.getGroups();
        }

        toDoList = toDoViewModel.getToDoList();
        if (toDoList.size() == 0){
            this.genererToDoList();
            toDoList = toDoViewModel.getToDoList();
        }

        //ListView
        toDoListView = (ListView) findViewById(R.id.listView);

        //Adapter
        toDoAdapter = new ToDoAdapter(MainActivity.this, toDoList, groups);
        toDoListView.setAdapter(toDoAdapter);

        popUpAjouter = new PopUpAjouter(this);
        groupListView = popUpAjouter.getListViewGroup();
        groupAdapter = new GroupAdapter(MainActivity.this, groups);
        groupListView.setAdapter(groupAdapter);

        //other

        trieTodo = new TrieTodo();
        filtre = false;
        indiceFiltre = 0;
        buttonFiltre = findViewById(R.id.buttonFiltre);



        //Initialisation d'une popup pour le suppression d'un item de la list
        final AlertDialog.Builder popUp1 = new AlertDialog.Builder(this);
        //Detext si clique sur un item
        toDoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //recuperation du todo en question
                final ToDo toDo = toDoList.get(position);
                //Parametrage de la popUp
                popUp1.setTitle("Supprimer la tâche " + toDo.getTitle());
                popUp1.setMessage("Voulez vous vraiment supprimer la tâche : " + toDo.getTitle()+ " ?");
                //Action bouton oui
                popUp1.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        toDoViewModel.delete(toDo);
                        toDoList.remove(toDo);
                        trieTodo.trier(toDoList,toDoAdapter);
                        Toast.makeText(getApplicationContext(), "La tâche " + toDo.getTitle() + " a étais supprimé", Toast.LENGTH_LONG).show();
                    }
                });
                //Action bouton non
                popUp1.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "Annulé", Toast.LENGTH_LONG);
                    }
                });
                //Affichage de la popUp
                popUp1.show();
            }
        });


    }
    //Gestion du filtre par group
    public void filtre(View view){
        //fait le tours de la list des groups pour revenir sur all une fois tous les groups passés
        if(!filtre){
            filtre = true;
            indiceFiltre = 0;
            this.actualiserFiltre();
            buttonFiltre.setText(groups.get(indiceFiltre).getColor());

        }else {
            if (indiceFiltre == groups.size()-1){
                filtre = false;
                this.actualiserFiltre();
                buttonFiltre.setText("All");
            }else {
                indiceFiltre ++;
                this.actualiserFiltre();
                buttonFiltre.setText(groups.get(indiceFiltre).getColor());
                }
        }

    }

    private void actualiserFiltre(){
        if(filtre){
            toDoList.clear();
            toDoList.addAll(toDoViewModel.getListToDo(groups.get(indiceFiltre).getId()));
        }else {
            toDoList.clear();
            toDoList.addAll(toDoViewModel.getToDoList());
        }
        toDoAdapter.notifyDataSetChanged();
    }

    public void popUpAjouter(View view){

        //Parametrage de la popup
        popUpAjouter.setTitle("Ajouter une tâche.");
        popUpAjouter.build();

        //Button cancel
        popUpAjouter.getAnnulerButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpAjouter.dismiss();
            }
        });

        //ChangeGroup
        groupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                groupChoisie = groups.get(position);
                popUpAjouter.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor(groupChoisie.getColor())));

            }

        });

        //Button ok
        popUpAjouter.getAjouterButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titre = popUpAjouter.getTitreToDoEdit().getText().toString();
                String description = popUpAjouter.getDecriptionToDoEdit().getText().toString();
                String priorityString = popUpAjouter.getPriorityToDoEdit().getText().toString();

                int priority;
                if(priorityString.trim().length() == 0){
                    priority = 0;
                }else {
                    priority = Integer.parseInt(priorityString);
                }

                if(titre.equals("")){
                    Toast.makeText(getApplicationContext(), "Tu n'a pas entrer de titre", Toast.LENGTH_LONG).show();
                    return;
                }
                ToDo toDo = new ToDo(titre, description, priority,groupChoisie.getId());

                toDoViewModel.insert(toDo);
                toDoList.add(toDo);

                //actualiser le filtre
                if(filtre){
                    toDoList.clear();
                    toDoList.addAll(toDoViewModel.getListToDo(groups.get(indiceFiltre).getId()));
                }else {
                    toDoList.clear();
                    toDoList.addAll(toDoViewModel.getToDoList());
                }
                toDoAdapter.notifyDataSetChanged();


                trieTodo.trier(toDoList, toDoAdapter);


                Toast.makeText(getApplicationContext(), "La tâche a était ajouté" + toDo.getTitle() + " " + toDo.getToDoIdGroup() + " " + groupChoisie.getColor(), Toast.LENGTH_LONG).show();
                popUpAjouter.dismiss();
                popUpAjouter.getTitreToDoEdit().getText().clear();
                popUpAjouter.getDecriptionToDoEdit().getText().clear();
                popUpAjouter.getPriorityToDoEdit().getText().clear();
                groupChoisie = groupDefault;
                popUpAjouter.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor(groupDefault.getColor())));

            }
        });
    }


    //Demande de confirmation Via popUp
    public void deleteAll(View view){
        final AlertDialog.Builder popUp = new AlertDialog.Builder(this);
        popUp.setTitle("Delete All");
        popUp.setMessage("Voulez vous vraiment supprimer toutes les tâche ?");
        popUp.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                toDoViewModel.deleteAll();
                toDoList.clear();
                toDoAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Toutes les tâches on était supprimées", Toast.LENGTH_LONG);
            }
        });
        popUp.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Annulé", Toast.LENGTH_LONG);
            }
        });
        popUp.show();

    }
    public void trieParTitre(View view){
        trieTodo.trier(toDoList,toDoAdapter,0);
    }

    public void trieParPriority(View view){
        trieTodo.trier(toDoList,toDoAdapter,1);
    }

    //methods priver pour structurer le code

    private void ajouter(ToDo toDo){
        toDoViewModel.insert(toDo);
        toDoList.add(toDo);
        trieTodo.trier(toDoList, toDoAdapter);
        toDoAdapter.notifyDataSetChanged();
    }

    private void ajouter(Group group){
        groupViewModel.insert(group);
        groups = groupViewModel.getGroups();
    }

    private void genererToDoList(){
        int nbToDoPerGroup = 3;
        int indiceToDo = 0;
        for (Group group: groups){
            for (int i=0; i < nbToDoPerGroup; i++){
                toDoViewModel.insert(new ToDo("Todo "+ indiceToDo,"description "+ indiceToDo,(int)( Math.random()*( 100 - 0 + 1 ) ) + 0,group.getId()));
                indiceToDo ++;
            }
        }
    }

    private List<Group> genererGroups(){
        List<Group> groups = new ArrayList<>();
        groups.add(new Group("Gray"));
        groups.add(new Group("Magenta"));
        groups.add(new Group("Navy"));
        groups.add(new Group("Olive"));
        return groups;
    }

    private void afficherToDoList(){
        for(ToDo toDo : toDoList){
            Log.i("ToDoList1", toDo.toString());
        }
    }


}