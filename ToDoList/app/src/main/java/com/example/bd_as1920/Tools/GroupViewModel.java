package com.example.bd_as1920.Tools;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.bd_as1920.Models.Group;

import java.util.List;

//ViewModel de l'entité Group

public class GroupViewModel extends AndroidViewModel {
    private GroupRepository groupRepository;

    public GroupViewModel(@NonNull Application application){
        super(application);
        groupRepository = new GroupRepository(application);
    }

    public Long insert(Group group){
        return groupRepository.insert(group);
    }


    public void update(Group group){
        groupRepository.update(group);
    }

    public void delete(Group group){
        groupRepository.delete(group);
    }

    public void deleteAll(){
        groupRepository.deleteAll();
    }
    public List<Group> getGroups(){ return groupRepository.getGroups();}
}
