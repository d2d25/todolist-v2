package com.example.bd_as1920.Tools;


import com.example.bd_as1920.Models.ToDo;

import java.util.Collections;
import java.util.List;

//Outil de Trie d'une List<ToDo> toDoList
//Il y avais moyen de faire une gestion des erreurs mais on a utiliser des booleans par manque de temp
public class TrieTodo {
    //Le numero corespond a un type de trie
    // 0 --> Trie par titre  odre croissant
    //1 --> Trie par priorité  odre decroissant
    private int numero;

    public TrieTodo(int numero) {
        this.numero = numero;
    }

    public TrieTodo() {
    }

    public int getTrie(){
        return this.numero;
    }

    public String getTrieString(){
        switch (this.numero){
            case 0:
                return "nom";
            case 1:
                return "priority";
            default:
                return "error";
        }
    }
    public boolean setTrie(int numero){
        if(numero >= 0 && numero <= 1){
            this.numero = numero;
            return true;
        }else{
            return false;
        }

    }

    public boolean setTrieString(String trie){
        switch (trie){
            case "nom" :
                this.numero = 0;
                return true;
            case "priority" :
                this.numero = 1;
                return true;
            default:
                return false;
        }
    }

    public void trier(List<ToDo> toDoList, ToDoAdapter adapter){
        switch (this.numero){
            case 0:
                Collections.sort(toDoList, ToDo.ComparatorTitle);
                break;
            case 1:
                Collections.sort(toDoList, ToDo.ComparatorPriority);
                break;
        }
        adapter.notifyDataSetChanged();
    }

    public boolean trier(List<ToDo> toDoList, ToDoAdapter adapter, int numero){
        boolean res = this.setTrie(numero);
        if(res){
            this.trier(toDoList, adapter);
        }
        return res;
    }

    public boolean trier(List<ToDo> toDoList, ToDoAdapter adapter, String trie){
        boolean res = this.setTrieString(trie);
        if(res){
            this.trier(toDoList, adapter);
        }
        return res;
    }

}
