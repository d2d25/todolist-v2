package com.example.bd_as1920.Models;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

//Entité Group composé d'une id et d'un String qui correspond a la couleur de ce groupe
@Entity(tableName = "Group_table")
public class Group {
    @PrimaryKey(autoGenerate = true)
    public int groupId;

    private String color;

    public Group(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return groupId;
    }

    public void setId(int id) {
        this.groupId = id;
    }

    @Override
    public String toString(){
        return "Group {" +
                " id = " + groupId +
                " Couleur = " + color +
                " }";
    }

    //Override de equals et hashCode pour l'utilisation de contains

    @Override
    public boolean equals(Object o){
        if(this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Group group = (Group) o;
        return color.equals(group.color);
    }
    @Override
    public int hashCode(){
        return color.hashCode();
    }
}
