package com.example.bd_as1920.Tools;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.bd_as1920.Models.ToDo;

import java.util.List;

//ViewModel de l'entité ToDo

public class ToDoViewModel extends AndroidViewModel {
    private ToDoRepository toDoRepository;
    LiveData<Integer> nbToDoListLD;
    LiveData<List<ToDo>> toDoListLD;

    public ToDoViewModel(@NonNull Application application) {
        super(application);
        toDoRepository = new ToDoRepository(application);
        nbToDoListLD = toDoRepository.getNbToDoListLD();
        toDoListLD = toDoRepository.getToDoListLD();
    }


    public Long insert(ToDo toDo){
        return toDoRepository.insert(toDo);
    }


    public void update(ToDo toDo){
        toDoRepository.update(toDo);
    }

    public void delete(ToDo toDo){
        toDoRepository.delete(toDo);
    }

    public void deleteAll(){
        toDoRepository.deleteAll();
    }

    public Integer nbToDoList(){
        return toDoRepository.count();
    }

    public List<ToDo> getToDoList(){
        return toDoRepository.getToDoList();
    }

    public List<ToDo> getListToDo(int groupId){
        return toDoRepository.getListToDo(groupId);
    }

    public LiveData<Integer> getNbToDoListLD() {
        return nbToDoListLD;
    }

    public LiveData<List<ToDo>> getToDoListLD() {
        return toDoListLD;
    }

}